// 1.Це конструкція, вона дозволяє  виконувати блок коду багаторазово, без необхідності писати той самий код вручну знову і знову.
// 2.Наприклад, можно використовувати цикл for для того, щоб пройтись по всьому вмісту масиву і виконати операцію над кожним елементом.
// 3.Явне перетворення типів даних означає процес перетворення значення з одного типу даних в інший за допомогою функції наприклад, Number(), String().
// Неявне перетворення типу даних відноситься до автоматичного перетворення типу.

let num = prompt("Please enter a number:");
let maxNum = parseInt(num);


if (isNaN(maxNum)) {
  console.log("Invalid input. Please enter a valid number.");
} else {
  
  const divisibleBy5 = [];

  
  for (let i = 0; i <= maxNum; i++) {
    if (i % 5 === 0) {
  
      divisibleBy5.push(i);
    }
  }

  if (divisibleBy5.length > 0) {
    console.log(`Numbers divisible by 5 from 0 to ${maxNum}:`);
    console.log(divisibleBy5);
  } 
  else {
    console.log("Sorry, no numbers.");
  }
}